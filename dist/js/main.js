$('.main_first_slider').slick({
    nextArrow: '<img class="arrows_slider1 arrow_right" src="./img/left-arrow-slider.png">',
    prevArrow: '<img class="arrows_slider1 arrow_left" src="./img/left-arrow-slider.png">',
    infinite: true,
    dots: true,
    autoplay: true,
    autoplaySpeed: 5000,
});

$(".slick-dots").wrap("<div class='cont-slick'></div>");
$(".cont-slick").wrap("<div class='custom-cont-dots'></div>");

$('.projects-main-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: '<img class="arrows_slider2 arrow_right" src="./img/arrow-slider-right.png">',
    prevArrow: '<img class="arrows_slider2 arrow_left" src="./img/arrow-slider-right.png">',
    fade: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    asNavFor: '.projects-preview-slider'
});
$('.projects-preview-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.projects-main-slider',
    dots: false,
    arrows: false,
    infinite: true,
    focusOnSelect: true,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
        {
            breakpoint: 770,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 375,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
});

$('.slider-news').slick({
    dots: true,
    slidesToShow: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
});

/* Навигация по странице 
—---------------------------------------------------— */
$('.mob-button').click(function () {
    $('.mob-menu').addClass('nav-open');
    $('body').addClass('open-mob-menu');
    return false;
});


$('.menu-close').click(function () {
    $('.mob-menu').removeClass('nav-open');
    $('body').removeClass('open-mob-menu');
    return false;
});


$(function () {
    $(document).click(function (event) {
        if ($('.mob-menu').hasClass('nav-open')) {
            if ($(event.target).closest('.mob-menu').length) {
                return;
            }
            $('.mob-menu').removeClass('nav-open');
            $('body').removeClass('open-mob-menu');
            event.stopPropagation();
        }
    });
});

$('.main-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    asNavFor: '.slider-preview'
});

$('.slider-preview').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.main-slider',
    nextArrow: '<img class="arrows_slider4 arrow_right" src="./img/arrow-slider-right.png">',
    prevArrow: '<img class="arrows_slider4 arrow_left" src="./img/arrow-slider-right.png">',
    dots: false,
    infinite: true,
    focusOnSelect: true,
    autoplay: true,
    autoplaySpeed: 5000,
    mobileFirst: true,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 540,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 300,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
    ]
});
